// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBU8qFTD78rGX5ijjs3ZybSIBFQS_x0yUE",
    authDomain: "empcrud-e96ce.firebaseapp.com",
    databaseURL: "https://empcrud-e96ce.firebaseio.com",
    projectId: "empcrud-e96ce",
    storageBucket: "empcrud-e96ce.appspot.com",
    messagingSenderId: "331660198426",
    appId: "1:331660198426:web:5dec1c285ab6f6ea8987a3",
    measurementId: "G-DL7PNNYNLR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
