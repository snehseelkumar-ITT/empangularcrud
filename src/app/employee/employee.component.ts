import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor( public employeeService: EmployeeService) { }
  submitted: boolean;
  showSucessMessage: boolean;
  formControls = this.employeeService.form.controls;

  ngOnInit(): void {
  }

  onSubmit(){
    this.submitted = true;
    if(this.employeeService.form.valid){
      if(this.employeeService.form.get('$key').value == null){
        this.employeeService.insertEmployee(this.employeeService.form.value);
      }
      else{
        this.employeeService.updateEmployee(this.employeeService.form.value);
      }
      this.submitted = false;
      this.showSucessMessage = true;
      setTimeout(()=> this.showSucessMessage = false, 3000);
      this.employeeService.form.reset();
    }
  }
}
