import { Component, OnInit } from '@angular/core';

import { EmployeeService } from '../shared/employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor( private employeeService: EmployeeService ) { }
  employeeArray = [];
  showDeleteMessage: boolean;
  searchText: string = "";

  ngOnInit(): void {
    this.employeeService.getEmployee().subscribe(
      list=> {
        this.employeeArray = list.map(item => {
          return {
            //converting observable into an array
            $key: item.key,
            ...item.payload.val()
          }
        })
      }
    );
  }

  onDelete($key){
    if(confirm('Are you sure you want to delete this record ?')){
      this.employeeService.deleteEmployee($key);
      this.showDeleteMessage = true;
      setTimeout(()=> this.showDeleteMessage = false, 3000);
    }
  }

  filterCondition(employee){
    return employee.fullName.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }
}
